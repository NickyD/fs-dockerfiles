set PATH=c:\cygwin64\bin;C:\Python27\Scripts;%PATH%
set AUTOBUILD_VSVER=150
set AUTOBUILD_VARIABLES_FILE=c:\build\fs-build-variables\variables
set AUTOBUILD_PKG_URL=http://192.168.1.115/dev/pkg

cd build
git clone https://vcs.firestormviewer.org/fs-build-variables
git clone https://vcs.firestormviewer.org/phoenix-firestorm

cd phoenix-firestorm
autobuild configure -c ReleaseFS -A%BUILD_ARCH% -v -- %BUILD_EXTRA_OPTS% --package
autobuild build -c ReleaseFS -A%BUILD_ARCH% -v --no-configure
