#!/bin/sh

git clone https://vcs.firestormviewer.org/phoenix-firestorm
git clone https://vcs.firestormviewer.org/fs-build-variables
export AUTOBUILD_VARIABLES_FILE=/fs-build-variables/variables
#export AUTOBUILD_PKG_URL=http://192.168.1.115/dev/pkg

cd phoenix-firestorm
autobuild configure -v -c ReleaseFS -A64 -- --package --ninja --no-opensim
cd build-linux-x86_64
ninja
